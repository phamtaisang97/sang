const URL = '/comments';
import axios from "axios";
export const state = () => {
    return {
        comments: [],
    }
}

export const mutations = {
    SET_COMMEMT(state, comment) {
        state.comments = comment
    },
    ADD_COMMENT(state, comment) {
        state.comments = comment
    }

}

export const actions = {
    loadData({ commit }) {
        axios.get(URL).then((response) => {
            commit('SET_COMMEMT', response.data.data)
        })
    },
    async createComments({ commit }, { data }) {
        console.log(data);
        const res = { isSuccess: false }
        try {
            const comment = await this.$axios.$post(URL, data)
            commit('ADD_COMMENT', {
                comment: comment
            })
            res.isSuccess = true
            res.data = comment
        } catch (error) {}
        return res
    },
}

export const getters = {
    get_all(state) {
        return state.comments
    }
}