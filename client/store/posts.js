const URL = 'http://127.0.0.1:4000/posts';
import axios from "axios";
export const state = () => {
    return {
        posts: [],
    }
}

export const mutations = {
    SET_PORTS(state, posts) {
        state.posts = posts
    },
    ADD_POST(state, posts) {
        state.posts = posts
    }

}

export const actions = {
    loadData({ commit }) {
        axios.get(URL).then((response) => {
            commit('SET_PORTS', response.data.data)
        })
    },
    async createPosts({ commit }, { data }) {
        const res = { isSuccess: false }
        console.log("data = ", data);
        try {
            let formData = new FormData();
            for (let i in data) {
                if (i === 'images') {
                    console.log('image', data[i].raw)
                    let images = data[i];
                    for (let j in images) {
                        formData.append('images[]', images[j].raw);
                    }
                } else {
                    formData.append(i, data[i]);
                }
            }


            const posts = await this.$axios.$post(URL, formData, {
                headers: { "Content-Type": "multipart/form-data" }
            })
            commit('ADD_POST', {
                posts: posts
            })
            res.isSuccess = true
            res.data = posts
        } catch (error) {}
        return res
    },
}

export const getters = {
    get_all(state) {
        return state.posts
    }
}