export default {
    getAllUsers (state) {
      return state.users
    },
    getUserById: (state) => (id) => {
      return state.users.find(users => users.id === id)
    }
  }