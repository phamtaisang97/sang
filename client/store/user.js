const URL = 'http://localhost:4000/users';
import axios from "axios";
export const state = () => {
    return {
        users: [],
        loading: true,
    }
}
export const mutations = {
    SET_USERS(state, posts) {
        state.posts = posts
    },
    changeLoadingState(state, loading) {
        state.loading = loading
    }
}
export const actions = {
    loadData({}) {
        axios.get(URL).then((response) => {
            console.log(response.data, this)
            commit('SET_USERS', response.data)
            commit('changeLoadingState', false)
        })
    }
}