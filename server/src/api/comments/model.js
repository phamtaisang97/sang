import mongoose, { Schema } from 'mongoose'

const commentsSchema = new Schema({
    content: {
        type: String
    },
    parents_comment: { type: Schema.Types.ObjectId, ref: 'Comments' },

    user: { type: Schema.Types.ObjectId, ref: 'User' }
}, {
    timestamps: true,
    toJSON: {
        virtuals: true,
        transform: (obj, ret) => { delete ret._id }
    }
});

commentsSchema.virtual('parents', {
    ref: 'Comments', //The Model to use
    localField: 'parents_comment', //Find in Model, where localField 
    foreignField: '_id', // is equal to foreignField
});

commentsSchema.methods = {
    view(full) {
        const view = {
            // simple view
            id: this.id,
            content: this.content,
            user: this.user,
            post: this.post,
            parents_comment: this.parents_comment,
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        }

        return full ? {
            ...view
            // add properties for a full view
        } : view
    }
}

const model = mongoose.model('Comments', commentsSchema)

export const schema = model.schema
export default model