import { success, notFound, error } from '../../services/response/'
import { Comments } from '.'
import { model as Posts } from '../posts/model'

export const create = ({ bodymen: { body } }, res, next) => {
    console.log(body);
    return Comments.create(body)
        .then((comment) => {
            let id_comment = comment._id;
            return Posts.findByIdAndUpdate(body.post, { $addToSet: { comments: id_comment } }, {}, function(err, doc) {
                if (err) {
                    return error(res, 500)
                }
                return success(res, 201)
            })
        })
        .then(success(res, 201))
        .catch(next);

}

export const index = async({ querymen: { query, select, cursor } }, res, next) => {
    var comments = await Comments.find(query, select, cursor).populate(["user", "parents"]);
    return res.json({ data: comments })
}

// get sub comments
export const getSubComment = async({ params }, res, next) => {
    var comments = await Comments.find({
        parents_comment: params.id
    }).populate(["user", "parents"]);
    return res.json({ data: comments })
}

export const show = async({ params }, res, next) => {
    var comments = await Comments.findById(params.id).populate(["user"]);
    return res.json({ data: comments })
}

export const update = ({ bodymen: { body }, params }, res, next) =>
    Comments.findById(params.id)
    .then(notFound(res))
    .then((comments) => comments ? Object.assign(comments, body).save() : null)
    .then((comments) => comments ? comments.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
    Comments.findById(params.id)
    .then(notFound(res))
    .then((comments) => comments ? comments.remove() : null)
    .then(success(res, 204))
    .catch(next)