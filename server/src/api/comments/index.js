import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, getSubComment } from './controller'
import { schema } from './model'
export Comments, { schema }
from './model'

const router = new Router()
const { content, user, post, parents_comment } = schema.tree

/**
 * @api {post} /comments Create comments
 * @apiName CreateComments
 * @apiGroup Comments
 * @apiParam title Comments's title.
 * @apiParam text Comments's text.
 * @apiSuccess {Object} comments Comments's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Comments not found.
 */
router.post('/',
    body({ content, user, post, parents_comment }),
    create)

router.get('/',
    query(),
    index)

router.get('/:id',
    show)

router.get('/:id/sub',
    getSubComment)

router.put('/:id',
    body({ content, user }),
    update)

router.delete('/:id',
    destroy)

export default router