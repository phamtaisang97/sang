import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Comments } from '.'

const app = () => express(apiRoot, routes)

let comments

beforeEach(async () => {
  comments = await Comments.create({})
})

test('POST /comments 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ title: 'test', text: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.text).toEqual('test')
})

test('GET /comments 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body.rows)).toBe(true)
  expect(Number.isNaN(body.count)).toBe(false)
})

test('GET /comments/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${comments.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(comments.id)
})

test('GET /comments/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /comments/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${comments.id}`)
    .send({ title: 'test', text: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(comments.id)
  expect(body.title).toEqual('test')
  expect(body.text).toEqual('test')
})

test('PUT /comments/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ title: 'test', text: 'test' })
  expect(status).toBe(404)
})

test('DELETE /comments/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${comments.id}`)
  expect(status).toBe(204)
})

test('DELETE /comments/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
