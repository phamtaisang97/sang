import { Comments } from '.'

let comments

beforeEach(async () => {
  comments = await Comments.create({ title: 'test', text: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = comments.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(comments.id)
    expect(view.title).toBe(comments.title)
    expect(view.text).toBe(comments.text)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = comments.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(comments.id)
    expect(view.title).toBe(comments.title)
    expect(view.text).toBe(comments.text)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
