import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, getComments } from './controller'
import { schema } from './model'
export Posts, { schema }
from './model'

const router = new Router()
const { title, content, user, comments, images } = schema.tree

router.post('/',
    body({
        title,
        content,
        user: {},
        comments: [],
        images: []
    }), create)

router.get('/',
    query(),
    index)

// get comment by id posts
router.get('/:id/comments',
    getComments)

router.get('/:id',
    show)

router.put('/:id',
    body({ title, content, user, comments, images }),
    update)

router.delete('/:id',
    destroy)

export default router