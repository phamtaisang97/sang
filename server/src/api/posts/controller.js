import { success, notFound } from '../../services/response/'
import { model as Posts } from './model'
import { model as Comments } from '../comments/model'
import multer from 'multer'
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "public/data/uploads/");
    },
    filename: (req, file, cb) => {
        console.log(file.originalname);
        cb(null, file.originalname);
    },
});
var upload = multer({ storage: storage })
export const create = [
    upload.array('images[]', 10),
    async(req, res, next) => {
        try {
            if (req.files == undefined) {
                Posts.create({
                        "title": req.body.title,
                        "user": req.body.user,
                        "images": [],
                        "comments": [],
                    })
                    .then((posts) => posts.view(true))
                    .then(success(res, 201))
                    .catch(next)
                return res.status(400).send({ message: "Please upload a file!" });
            }
            // SEND FILE TO CLOUDINARY
            const cloudinary = require('cloudinary').v2
            cloudinary.config({
                cloud_name: 'sangpt',
                api_key: '953835344741625',
                api_secret: 'vYegK0uxaJ-IcL3NL2-YAmBJ71w'
            })
            const files = req.files
            const uniqueFilename = new Date().toISOString()
            let images = [];
            for (let i in files) {
                let path = files[i].path;
                await cloudinary.uploader.upload(
                    path, { public_id: `posts/${uniqueFilename}`, tags: `posts` }, // directory and tags are optional
                    function(err, image) {
                        if (err) return res.send(err)
                        const fs = require('fs')
                        fs.unlinkSync(path)
                        images.push(image.url)
                    }
                )
            }
            Posts.create({
                    "title": req.body.title,
                    "user": req.body.user,
                    "images": images,
                    "comments": [],
                })
                .then((posts) => posts.view(true))
                .then(success(res, 201))
                .catch(next)
        } catch (err) {
            res.status(500).send({
                message: `Could not upload the file: ${req.file.originalname}. ${err}`,
            });
        }
    }
]
export const index = async({ querymen: { query, select, cursor } }, res, next) => {
    var posts = await Posts.find(query, select, cursor).populate([{
        path: 'comments',
        populate: {
            path: 'user',
            model: 'User'
        }
    }, "user"]);
    return res.json({ data: posts })
}

export const show = async({ params }, res, next) => {
    var posts = await Posts.findById(params.id).populate([{
        path: 'comments',
        populate: {
            path: 'user',
            model: 'User'
        }
    }, "user"]);

    return res.json({ data: posts })
}

// get comments
export const getComments = async({ params }, res, next) => {
    var comments = await Comments.find({
        post: params.id
    });
    console.log(comments);
    return res.json({ data: comments })
}

export const update = ({ bodymen: { body }, params }, res, next) =>
    Posts.findById(params.id)
    .then(notFound(res))
    .then((posts) => posts ? Object.assign(posts, body).save() : null)
    .then((posts) => posts ? posts.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
    Posts.findById(params.id)
    .then(notFound(res))
    .then((posts) => posts ? posts.remove() : null)
    .then(success(res, 204))
    .catch(next)