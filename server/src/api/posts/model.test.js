import { Posts } from '.'

let posts

beforeEach(async () => {
  posts = await Posts.create({ title: 'test', content: 'test', id_user: 'test', id_post_detail: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = posts.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(posts.id)
    expect(view.title).toBe(posts.title)
    expect(view.content).toBe(posts.content)
    expect(view.id_user).toBe(posts.id_user)
    expect(view.id_post_detail).toBe(posts.id_post_detail)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = posts.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(posts.id)
    expect(view.title).toBe(posts.title)
    expect(view.content).toBe(posts.content)
    expect(view.id_user).toBe(posts.id_user)
    expect(view.id_post_detail).toBe(posts.id_post_detail)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
