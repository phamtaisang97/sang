import mongoose, { Schema } from 'mongoose'

const postsSchema = new Schema({
    title: {
        type: String
    },
    content: {
        type: String
    },
    user: { type: Schema.Types.ObjectId, ref: 'User' },
    comments: [{ type: Schema.Types.ObjectId, ref: "Comments" }],
    images: []
}, {
    timestamps: true
})

// console.log(postsSchema.methods);
postsSchema.methods = {
    view(full) {
        const view = {
            // simple view
            id: this.id,
            title: this.title,
            content: this.content,
            user: this.user,
            comments: [this.comments],
            images: [],
            createdAt: this.createdAt,
            updatedAt: this.updatedAt
        }
        return full ? {
            ...view
            // add properties for a full view
        } : view
    }
}

export const model = mongoose.model('Posts', postsSchema)

export const schema = model.schema
    // export default model