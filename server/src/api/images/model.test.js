import { Images } from '.'

let images

beforeEach(async () => {
  images = await Images.create({ images: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = images.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(images.id)
    expect(view.images).toBe(images.images)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = images.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(images.id)
    expect(view.images).toBe(images.images)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
