import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, getListFiles, download } from './controller'
import { schema } from './model'
export Images, { schema }
from './model'

const router = new Router()
const { images } = schema.tree
    /**
     * @api {post} /images Create images
     * @apiName CreateImages
     * @apiGroup Images
     * @apiParam images Images's images.
     * @apiSuccess {Object} images Images's data.
     * @apiError {Object} 400 Some parameters may contain invalid values.
     * @apiError 404 Images not found.
     */
router.post('/',
    create)

/**
 * @api {get} /images Retrieve images
 * @apiName RetrieveImages
 * @apiGroup Images
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of images.
 * @apiSuccess {Object[]} rows List of images.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */

router.get('/files/:id',
    download)

/**
 * @api {put} /images/:id Update images
 * @apiName UpdateImages
 * @apiGroup Images
 * @apiParam images Images's images.
 * @apiSuccess {Object} images Images's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Images not found.
 */

router.get('/file',
    getListFiles)

/**
 * @api {get} /images/:id Retrieve images
 * @apiName RetrieveImages
 * @apiGroup Images
 * @apiSuccess {Object} images Images's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Images not found.
 */

router.get('/',
    query(),
    index)

/**
 * @api {get} /images/:id Retrieve images
 * @apiName RetrieveImages
 * @apiGroup Images
 * @apiSuccess {Object} images Images's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Images not found.
 */
router.get('/:id',
    show)

/**
 * @api {put} /images/:id Update images
 * @apiName UpdateImages
 * @apiGroup Images
 * @apiParam images Images's images.
 * @apiSuccess {Object} images Images's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Images not found.
 */
router.put('/:id',
    body({ images }),
    update)

/**
 * @api {delete} /images/:id Delete images
 * @apiName DeleteImages
 * @apiGroup Images
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Images not found.
 */
router.delete('/:id',
    destroy)

export default router