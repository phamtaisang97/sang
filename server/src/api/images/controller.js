import { success, notFound } from '../../services/response/'
import { Images } from '.'
import multer from 'multer'

let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "public/data/uploads/");
    },
    filename: (req, file, cb) => {
        console.log(file.originalname);
        cb(null, file.originalname);
    },
});

var upload = multer({ storage: storage })

export const create = [
    upload.single('images'),
    async(req, res, next) => {
        try {
            if (req.file == undefined) {
                return res.status(400).send({ message: "Please upload a file!" });
            }
            // console.log(req.file, req.body);
            // return;
            res.status(200).send({
                message: "Uploaded the file successfully: " + req.file.originalname,
            });

        } catch (err) {
            res.status(500).send({
                message: `Could not upload the file: ${req.file.originalname}. ${err}`,
            });
        }
    }
]

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
    Images.count(query)
    .then(count => Images.find(query, select, cursor)
        .then((images) => ({
            count,
            rows: images.map((images) => images.view())
        }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
    Images.findById(params.id)
    .then(notFound(res))
    .then((images) => images ? images.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
    Images.findById(params.id)
    .then(notFound(res))
    .then((images) => images ? Object.assign(images, body).save() : null)
    .then((images) => images ? images.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
    Images.findById(params.id)
    .then(notFound(res))
    .then((images) => images ? images.remove() : null)
    .then(success(res, 204))
    .catch(next)


export const getListFiles = (req, res) => {
    const directoryPath = "http://localhost:4000/public/data/uploads/";
    fs.readdir(directoryPath, function(err, files) {
        if (err) {
            res.status(500).send({
                message: "Unable to scan files!",
            });
        }

        let fileInfos = [];

        files.forEach((file) => {
            fileInfos.push({
                name: file,
                url: baseUrl + file,
            });
        });

        res.status(200).send(fileInfos);
    });
};

export const download = ({ params }, res, next) => {
    // const fileName = req.params.name;
    // const directoryPath = __basedir + "public/data/uploads/";

    // res.download(directoryPath + fileName, fileName, (err) => {
    //     if (err) {
    //         res.status(500).send({
    //             message: "Could not download the file. " + err,
    //         });
    //     }
    // });
};